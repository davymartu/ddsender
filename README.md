# DDSender

Trasmissione fax/email/sms all'interfaccia JSON API di DigitalDox.
Il programma recupera da una directory locale (oppure da una directory FTP) i files ZIP da trasmettere all'interfaccia DD. Un altro thread si occuper� di reperire gli esiti dalla medesima interfaccia e scriverli su database tramite JDBC.

## Struttura cartelle

  * **backup**	tutti i files zip processati vengono memorizzati qui (backup/anno/mese)
  * **config**	
    * **configuration.properties** Files di configurazione principale
    * **errors_map.txt** Mappa errori DigitalDox
    * **log4j2.xml** Configurazione logger
    * **libs** librerie di terze parti
    * **outFax** Cartella di default deposito files zip da inviare

### Configuration.properties

#### Job section 
* job.start=00\:01  -  orario HH:MM Inizio job
* job.end=23\:59 -  orario HH:MM Fine Job
* job.scan_timeout=10 -  frequenza scan (in secondi)
* job.close_at_midnight=false - chiudi a mezzanotte
* job.input_dir_path=outFax - path to scan
* job.jsonif.sender=true - abilitazione thread sender 
* job.jsonif.report=true - abilitazione thread esiti
* job.jsonif.report.scan_timeout=10 - frequenza scan esiti (in secondi)

#### FTP section 
* ftp.in.enable=true
* ftp.in.ip=192.168.66.166
* ftp.in.port=21
* ftp.in.pasv=true
* ftp.in.username=<MYFTPUSER>
* ftp.in.password=MYCODEDPASSWORD - password crittata 
* ftp.in.debug=true - abilita trace FTP
* ftp.in.folder=/work/iOmniaTransmitter/outFax - path remoto 

#### DD JSON Api section 
* dd.endpoint=https://externalws.d-documents.it/
* dd.username=<MYDDUSER>
* dd.password=<MYCODEDPASSWORD>
* dd.centrodicosto=<CDC>

#### JDBC esiti section
* jdbc.username=QUTENTE
* jdbc.password=MYCODEDPASSWORD
* jdbc.schema=SYSDUMMY1
* jdbc.host=192.168.66.166
* jdbc.databasename=
* jdbc.driver=toolbox - driver toolbox IBM nativo o JT400 (vedi propriet� JDBC)
* jdbc.toolbox_trace=false

### Encrypt password per file di configurazione configuration.properties

settare prima la chiave crittografica:

```
 	private static final String KEY = "<YOU 128BIT KEY>"; 
```

Eseguire main di PasswordUtils 

```
	/* ENCRYPT YOUR PASSWORD */
	public static void main(String[] args) {
		try {
			
			String pwd= encrypt("*CURRENT");
			System.out.println(pwd);
			System.out.println(decrypt(pwd));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
```
### Prerequisiti

* Java 8+
* Ant
* Maven


### Compilazione

Ecco i target di compilazione ANT:

* Recupero jar da Maven

```
ant resolve
```

* Rilascio (creazione struttura cartelle e JAR)

```
ant create_new_environment
```

* Solo JAR

```
ant create_only_jar
```


## Compilato con

* [Ant](https://ant.apache.org/) - Apache Ant


## Autori

* **Davide Martusciello** - *Initial work* - [davymartu](https://bitbucket.org/davymartu/)

----------
## Licenza
[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html) Copyright (C) M.T.S.Informatica s.r.l

