/*
 * Copyright (c) 2019 Davide Martusciello
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package martu.domain.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import martu.utils.Configurator;


public class Context {
	private final Logger logger = LogManager.getLogger(Context.class);
	private Strategy strategy;
	private Timer timer;
	private int seconds;
	private RemindTask rt;
	private String data;
	private SimpleDateFormat sdf;


	public Context(Strategy strategy,int seconds) {
		
		this.timer = new Timer();
		this.seconds=seconds;
		this.strategy = strategy;
	}

	public void executeStrategy() {
		sdf=new SimpleDateFormat("yyyy-MM-dd");
		data=sdf.format(new GregorianCalendar().getTime());
		rt=new RemindTask();
		timer.schedule(rt, 1,seconds*1000);
	}

	public void fermaTimer(){
		rt.cancel();
		timer.cancel();
		logger.info("Timer fermato...");
		System.exit(0);
	}

	public void ControlSwitchDate(){
		String dataAttuale=sdf.format(new GregorianCalendar().getTime());
		if (dataAttuale.equals(data)){
			return;
		}else{
			data=dataAttuale;
			if(Configurator.getInstance().getCloseAtMidnight()){
				ThreadContext.put("threadName","MainThread");
				logger.info("Impostata la chiusura notturna. Bye bye...");
				System.exit(0);
			}
		}
	}

	class RemindTask extends TimerTask {

		@Override
		public void run() {
			Calendar currentcal=Calendar.getInstance();
			ControlSwitchDate();
			if((currentcal.after(Configurator.getInstance().getInizioJob())&&
					currentcal.before(Configurator.getInstance().getFineJob()))){
//				FramePrincipale.getInstance().getControllerPanel().getTimerSlotCounter().setTimeout(SchedulerPanel.getInstance().getTimeout());
				strategy.execute();
			}

		}

	}
}
