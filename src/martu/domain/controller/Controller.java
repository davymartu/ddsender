/*
 * Copyright (c) 2019 Davide Martusciello
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package martu.domain.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
	private static Controller instance;
	private Set<Context> allJob;
	private static final Logger logger = LogManager.getLogger(Controller.class);
	
	public static Controller getInstance(){
		if(instance==null)
			instance=new Controller();
		return instance;
	}
	
	public Controller() {
		this.allJob = new HashSet<Context>();
	}

	public void avvia(Strategy strategy,int timeoutSeconds){
		logger.info("Avvio " + strategy.getClass().getSimpleName()  + " with scan_timeout = " + timeoutSeconds);
		Context context = new Context(strategy,timeoutSeconds);
		context.executeStrategy();
		allJob.add(context);
	}

	public void stop() {
		Iterator<Context> it = allJob.iterator();
		while(it.hasNext()) {
			it.next().fermaTimer();
		}		
	}

}
