/*
 * Copyright (c) 2019 Davide Martusciello
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package martu.domain.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.json.JSONException;
import kong.unirest.json.JSONObject;
import martu.persistenza.ArchivioMessaggi;
import martu.utils.Configurator;

public class JSONInterfaceReportStrategy implements Strategy {

	private final static Logger logger = LogManager.getLogger(JSONInterfaceReportStrategy.class);
	private final DateFormat defaultDateFmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	@Override
	public void execute() {
		ThreadContext.put("threadName","Reporter");
		logger.debug("Avvio JSONInterfaceReportStrategy task...");
		ArrayList<String> pendingArray = ArchivioMessaggi.GetFaxPending();
		Iterator<String> it = pendingArray.iterator();

		String ddEndpoint = Configurator.getInstance().getPropValue("dd.endpoint","");
		String ddUserName = Configurator.getInstance().getPropValue("dd.username","");
		String ddPassword = Configurator.getInstance().getPropValue("dd.password","");

		while(it.hasNext()) {

			String currentDDRef = it.next();
			logger.info("Sending request for job " + currentDDRef);
			try {
				Unirest.get(ddEndpoint + "getjob/" + currentDDRef)
				.basicAuth(ddUserName, ddPassword)
				.asString()
				.ifFailure(response -> {
					logger.warn("Error reading JSON response");
					logger.warn("status: " + response.getStatus());
					logger.warn("body: " + response.getBody());
					response.getParsingError().ifPresent(e -> {
						logger.warn("Parsing Exception: ", e);
						logger.warn("Original body: " + e.getOriginalBody());
					});
				})
				.ifSuccess(response -> processResponse(response,currentDDRef));
			}catch(Exception e) {
				logger.error(e.getMessage(), e);
			}


		}

		logger.debug("Fine JSONInterfaceSenderStrategy task...");
		ThreadContext.clearAll();
	}

	private Object processResponse(HttpResponse<String> response,String currentDDRef) {
		try {
			String bodyResponse = response.getBody();
			//String bodyResponse ="{\"success\":true,\"msg\":{\"jobnumber\":\"WW00050033\",\"recipient\":[{\"status\":null,\"txmdate\":\"2019-09-26T12:24:20.000Z\",\"retry_count\":1,\"address\":null,\"description\":\"WAIT\"}]}}";
			logger.debug(bodyResponse);

			JSONObject responseObject = new JSONObject(bodyResponse);
			if (responseObject.getBoolean("success")) {
				JSONObject recipient = responseObject.getJSONObject("msg").getJSONArray("recipient").getJSONObject(0);
				int status = recipient.optInt("status",-9999);
				String txmDateString =  recipient.optString("txmdate");
				int retryCount = recipient.optInt("retry_count");
				String description = recipient.optString("description");

				logger.debug("Readed status (-9999 is NULL/WAITING value): " + status );
				logger.debug("Readed txmdate: " + txmDateString);
				Date txmDate = new Date();
				try {
					txmDate = parseDate(txmDateString);
					logger.debug("Readed parsed txm_date: " + defaultDateFmt.format(txmDate));
				} catch (JSONException e) {
					logger.warn(e.getMessage(),e);
				} catch (ParseException e) {
					logger.warn(e.getMessage(),e);
				}
				logger.debug("Readed retry_count: " + retryCount );
				logger.debug("Readed description: " + description);
				if(status != -9999 ) {
					ArchivioMessaggi.UpdateFaxReport(currentDDRef, recipient.getInt("status"), txmDate, recipient.getInt("retry_count"),description);
				}else {
					logger.info(currentDDRef +" is in transmission");
				}
			}
		}catch(Exception e) {
			logger.error(e.getMessage(),e);
		}
		return null;
	}

	public static Date parseDate( String input ) throws java.text.ParseException {

		SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSSz" );

		//this is zero time so we need to add that TZ indicator for 
		if ( input.endsWith( "Z" ) ) {
			input = input.substring( 0, input.length() - 1) + "GMT-00:00";
			logger.debug(input);
		} else {
			int inset = 6;

			String s0 = input.substring( 0, input.length() - inset );
			String s1 = input.substring( input.length() - inset, input.length() );

			input = s0 + "GMT" + s1;
		}

		return df.parse( input );

	}

	public static String getCurrentTimezoneOffset() {

		TimeZone tz = TimeZone.getDefault();  
		Calendar cal = GregorianCalendar.getInstance(tz);
		int offsetInMillis = tz.getOffset(cal.getTimeInMillis());

		String offset = String.format("%02d:%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
		offset = (offsetInMillis >= 0 ? "+" : "-") + offset;

		return offset;
	}
	
}