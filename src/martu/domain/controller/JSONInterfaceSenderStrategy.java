/*
 * Copyright (c) 2019 Davide Martusciello
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package martu.domain.controller;

import java.io.File;
import java.net.ConnectException;
import java.util.ArrayList;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import martu.domain.services.FtpConnector;
import martu.persistenza.ArchivioMessaggi;
import martu.utils.Configurator;
import martu.utils.Copier;
import martu.utils.CreaCartelle;
import martu.utils.FileFilterExtensionAndFirstChar;

public class JSONInterfaceSenderStrategy implements Strategy {

	private final Logger logger = LogManager.getLogger(JSONInterfaceSenderStrategy.class);
	private CreaCartelle creatorBackupOutZip;
	@Override
	public void execute() {
		//Set up the context before getting logger
		ThreadContext.put("threadName","Sender");        
		logger.debug("Avvio JSONInterfaceSenderStrategy task...");

		ArrayList<String> commas=new ArrayList<String>();
		creatorBackupOutZip = new  CreaCartelle("backup/zip");

		for (char currentPrefixToProcess : Configurator.getInstance().getPropValue("job.prefix_to_process","F").toCharArray()) {
			logger.debug("Adding comma :" +String.valueOf(currentPrefixToProcess));
			commas.add(String.valueOf(currentPrefixToProcess));
		}		

		try {

			if(Configurator.getInstance().getPropValue("ftp.in.enable","false").equals("true")) {
				FtpConnector fc1 = new FtpConnector();
				ArrayList<String> ext=new ArrayList<String>();
				ext.add("zip");

				if(Configurator.getInstance().getPropValue("ftp.in.debug","").equals("true")) {
					fc1.enableDebug();
				}

				fc1.connect(Configurator.getInstance().getPropValue("ftp.in.ip",""),
						Integer.parseInt(Configurator.getInstance().getPropValue("ftp.in.port","21")),
						Configurator.getInstance().getPropValue("ftp.in.username",""),
						Configurator.getInstance().getPropValue("ftp.in.password",""),
						Boolean.parseBoolean(Configurator.getInstance().getPropValue("ftp.in.pasv","true")));
				fc1.scaricaFile(Configurator.getInstance().getPropValue("ftp.in.folder",""),Configurator.getInstance().getPropValue("job.input_dir_path","txt"),commas,ext,true);

				fc1.disconnetti();
			}
		} catch (ConnectException e) {
			logger.error(e.getMessage(),e);
			logger.error("errore di connessione al server FTP");			
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}

		logger.debug("Sending to REST interface...");

		String dirFilesToSend = Configurator.getInstance().getPropValue("job.input_dir_path","txt");
		File[] listOfZipFiles = new File(dirFilesToSend).listFiles(new FileFilterExtensionAndFirstChar("zip",commas));

		String ddEndpoint = "";
		String ddUserName = "";
		String ddPassword = "";
		
		if(listOfZipFiles.length  == 0) {
			logger.debug("no files in dir " + dirFilesToSend + ". nothing to do");			
		}else {
			ddEndpoint = Configurator.getInstance().getPropValue("dd.endpoint","");
			ddUserName = Configurator.getInstance().getPropValue("dd.username","");
			ddPassword = Configurator.getInstance().getPropValue("dd.password","");
			logger.debug("scan dir " + dirFilesToSend );
		}
		
		for(int i = 0; i < listOfZipFiles.length; i++){
			logger.info("Preparing " + listOfZipFiles[i].getName());
			File currentZipToSend = listOfZipFiles[i];
			try {
				Unirest.post(ddEndpoint + "sendjob")
				.basicAuth(ddUserName, ddPassword)
				.field("file", currentZipToSend) 
				.asString()
				.ifFailure(response -> {
					logger.warn("Error reading JSON response");
					logger.warn("status: " + response.getStatus());
					logger.warn("body: " + response.getBody());
					response.getParsingError().ifPresent(e -> {
						logger.warn("Parsing Exception: ", e);
						logger.warn("Original body: " + e.getOriginalBody());
					});
				})
				.ifSuccess(response -> processResponse(response,currentZipToSend));
			}catch(Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

		logger.debug("Fine JSONInterfaceSenderStrategy task...");
		ThreadContext.clearAll();
	}
	private void processResponse(HttpResponse<String> response, File file) {
		try {
			logger.debug(response.getBody());
			JSONObject responseObject = new JSONObject(response.getBody());
			if (responseObject.optBoolean("success")) {		    	
				String messageReference = responseObject.optString("msg");
				if(!messageReference.isEmpty()) {
					long mtsProtocolNumber = Long.parseLong(FilenameUtils.removeExtension(file.getName()).substring(1));
					ArchivioMessaggi.UpdateRefNumber(mtsProtocolNumber, messageReference);
				}else {
					logger.warn("No msg reference found!");	
				}

			}else {
				logger.warn("No success response, nothing to do!");
			}
		}catch(Exception e) {
			logger.error(e.getMessage(),e);			
		}finally {
			String backupZipPath = creatorBackupOutZip.getGeneratedDirPath() + file.getName();	
			if(backupZipPath != null && !backupZipPath.isEmpty()) {
				Copier.copy(file, new File(backupZipPath));
			}
			if(file.delete())
				logger.info(file+ " deleted.");
			else {
				logger.error(file+ " not deleted.FATAL ERROR. Exiting");
				System.exit(1);
			}
		}
	}
}