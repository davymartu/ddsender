/*
 * Copyright (c) 2019 Davide Martusciello
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package martu.persistenza;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import martu.utils.Configurator;

public class ArchivioMessaggi {

	public  static long as400Time;

	private static final Logger logger = LogManager.getLogger(ArchivioMessaggi.class);

	public static void UpdateRefNumber(long mtsProtocolNumber, String ddRefNumber){
		logger.info("Executing updateRefNumber...");

		PreparedStatement pstmt= null;

		try{
			Connection conn = As400DbConnection.getInstance().getDB();
			pstmt = conn.prepareStatement("UPDATE FTTES00F SET FTCARR = ? WHERE FTTYPE ='F' AND FTNPRO = ? AND FTSTAT = 'T'");			
			pstmt.setString(1,ddRefNumber );       
			pstmt.setLong(2,mtsProtocolNumber);
			int numUpd = pstmt.executeUpdate();			
			logger.debug(numUpd + " rows updated - Protocol Number:" + mtsProtocolNumber + " - DDRefNumber:" + ddRefNumber);			
		} catch (SQLException e) {
			logger.error( e.getMessage(),e);
		}finally{
			if(pstmt != null)
				try {
					pstmt.close();
				} catch (SQLException e) {
					logger.error( e.getMessage(),e);
				}
			As400DbConnection.getInstance().disconnetti();
		}
	}

	public static ArrayList<String> GetFaxPending(){
		logger.info("Executing GetFaxPending...");		
		ArrayList<String> pendingResult = new ArrayList<String>();
		PreparedStatement pstmt= null;
		ResultSet resultSet = null;
		try{
			Connection conn = As400DbConnection.getInstance().getDB();
			pstmt = conn.prepareStatement("SELECT TRIM(FTCARR) FROM FTTES00F WHERE  FTCARR <> '' AND FTTYPE ='F' AND FTSTAT = 'T'");
			resultSet = pstmt.executeQuery();
			long i = 0;
			while (resultSet.next()) {
				String carrierID = resultSet.getString(1);
				pendingResult.add(carrierID);
				logger.info("found " + carrierID);
				++i;
			}
			if(i == 0 ) {
				logger.info("No result found ");
			}
			resultSet.close();			
		} catch (SQLException e) {
			logger.error( e.getMessage(),e);
		}finally{
			if(pstmt != null)
				try {
					pstmt.close();
				} catch (SQLException e) {
					logger.error( e.getMessage(),e);
				}
			As400DbConnection.getInstance().disconnetti();
		}
		return pendingResult;
	}
	
	public static void UpdateFaxReport(String ddRefNumber, int status, Date txmDate, int retryCount,String description ){
		logger.info("Executing UpdateFaxReport...");
		SimpleDateFormat dataAs400 = new SimpleDateFormat( "yyyyMMdd" );
		SimpleDateFormat oraAs400 = new SimpleDateFormat( "HHmmss" );
		PreparedStatement pstmt= null;

		try{
			Connection conn = As400DbConnection.getInstance().getDB();
			pstmt = conn.prepareStatement("UPDATE FTTES00F SET FTDSND = ? , FTHSND = ? , FTTENT= ?, FTSTAT = ?, FTERRO = ? WHERE FTCARR = ?");			
			pstmt.setLong(1,Long.parseLong(dataAs400.format(txmDate) ));
			pstmt.setLong(2,Long.parseLong(oraAs400.format(txmDate) ));
			pstmt.setInt(3,retryCount);
			
			if(status == 0 ) {
				pstmt.setString(4,"C");
				pstmt.setString(5,"");
			}else {
				pstmt.setString(4,"E");
				if(Configurator.getInstance().getDigitalDoxErrors().get(status) != null) {
					pstmt.setString(5,Configurator.getInstance().getDigitalDoxErrors().get(status).substring(1,4));
				}else if (!description.isEmpty()){
					pstmt.setString(5,description.substring(0,3));	
				}else{
					pstmt.setString(5,"ERR");	
				}
			}			
			pstmt.setString(6,ddRefNumber);			
			int numUpd = pstmt.executeUpdate();			
			logger.debug(numUpd + " rows updated - DDRefNumber:" + ddRefNumber);			
		} catch (SQLException e) {
			logger.error( e.getMessage(),e);
		}finally{
			if(pstmt != null)
				try {
					pstmt.close();
				} catch (SQLException e) {
					logger.error( e.getMessage(),e);
				}
			As400DbConnection.getInstance().disconnetti();
		}
	}

}



