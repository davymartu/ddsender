/*
 * Copyright (c) 2019 Davide Martusciello
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package martu.persistenza;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import martu.utils.Configurator;


public class As400DbConnection {

	private static final Logger logger = LogManager.getLogger(As400DbConnection.class);
	private static As400DbConnection instance;

	//	private String libraries;
	private String host;	
	private String username=""; 
	private String password="";
	private String databaseName;
	private Properties jdbcProps;

	private static Connection db; 	
	private static boolean connesso = false;
	private boolean extendedMetada = false;
	private boolean isToolboxTraceActive;
	private String driver;
	private String libraries;

	private As400DbConnection() { 
		getSettings();
		connesso = false;
		jdbcProps = new Properties();	
		if(!username.isEmpty())
			jdbcProps.setProperty("user", username);
		if(!password.isEmpty())
			jdbcProps.setProperty("password", password);	
		jdbcProps.setProperty("libraries", libraries); // Non funziona c'� un bug che setta a 37 la prima chiamata nel toolbox: da segnalare
		jdbcProps.setProperty("naming", "sql");//system = nico
		jdbcProps.setProperty("errors", "full");
		if(isToolboxTraceActive) {
			jdbcProps.setProperty("toolbox trace", "all");
			jdbcProps.setProperty("trace", "true");
		}
		jdbcProps.setProperty("driver", driver);
		if(extendedMetada)
			jdbcProps.setProperty("extended metadata","true");

		if(databaseName != null && !databaseName.isEmpty())
			jdbcProps.setProperty("database name", databaseName);

		connetti();
	}

	public Properties getJdbcProps() {
		return jdbcProps;
	}

	public void setJdbcProps(Properties jdbcProps) {
		this.jdbcProps = jdbcProps;
	}

	public void setExtendedMetadata(boolean active){
		this.extendedMetada = true;
		jdbcProps.setProperty("extended metadata", String.valueOf(active));
		logger.info("extended metadata set to " + String.valueOf(active) );
	}

	public static synchronized As400DbConnection getInstance() {
		if (instance==null) 
			instance = new As400DbConnection();
		try {
			if(db.isClosed())
				instance= new As400DbConnection();
		} catch (SQLException e) {
			logger.error(e.getMessage(),e);
		}
		return instance;
	} 

	public boolean connetti() {
		if (connesso == false);
		try {					
			DriverManager.registerDriver(new com.ibm.as400.access.AS400JDBCDriver());
			String connectionString = "jdbc:as400://" + host ;
			if(driver.equals("native"))
				connectionString = "jdbc:db2:" + host;
			logger.info("connecting to  " + connectionString);			
			db = DriverManager.getConnection (connectionString ,jdbcProps);	
			//db.setSchema(Configurator.getInstance().getPropValue("jdbc.schema",""));			
			logger.info("AS Connesso");
			connesso = !db.isClosed();
		}catch (Exception e) { 
			logger.error(e.getMessage()); 
		}
		return connesso;
	}

	public void disconnetti() {
		try {
			db.close();
			connesso = false;
			logger.info("As400 disconnesso");
		} 
		catch (Exception e) { 
			logger.error(e.getMessage()); 
		}
	}

	public boolean isConnesso() { return connesso; }

	public Connection getDB(){
		return db;
	}

	private void getSettings(){			
		username = Configurator.getInstance().getPropValue("jdbc.username","");
		password = Configurator.getInstance().getPropValue("jdbc.password","");
		libraries = Configurator.getInstance().getPropValue("jdbc.libraries","");
		host = Configurator.getInstance().getPropValue("jdbc.host","*LOCAL");
		databaseName = Configurator.getInstance().getPropValue("jdbc.databasename","");
		isToolboxTraceActive = Boolean.parseBoolean(Configurator.getInstance().getPropValue("jdbc.toolbox_trace","false"));
		driver = Configurator.getInstance().getPropValue("jdbc.driver","toolbox");
	}



}