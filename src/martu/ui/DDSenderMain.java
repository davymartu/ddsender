package martu.ui;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.core.LoggerContext;

import kong.unirest.Unirest;
import martu.domain.controller.Controller;
import martu.domain.controller.JSONInterfaceReportStrategy;
import martu.domain.controller.JSONInterfaceSenderStrategy;
import martu.utils.Configurator;

public class DDSenderMain {

	private static final Logger logger = LogManager.getLogger(DDSenderMain.class);

	public static void main(String[] args) {
		LoggerContext context = (LoggerContext) LogManager.getContext(false);
		File file = new File("config/log4j2.xml");
		context.setConfigLocation(file.toURI());		
		ThreadContext.put("threadName","MainThread");
		try{
			@SuppressWarnings("resource")
			RandomAccessFile randomFile = new RandomAccessFile(".lock","rw");
			FileChannel channel = randomFile.getChannel();

			if(channel.tryLock() == null) 
				logger.error("Another instance already Running...exit!");
			else {
				logger.info("Avvio in corso...");
				
				int socketTimeout = Integer.parseInt(Configurator.getInstance().getPropValue("dd.sockettimeout","60000"));
				Unirest.config().socketTimeout(socketTimeout);
				
				if(Configurator.getInstance().getPropValue("job.jsonif.sender","false").equals("true"))
					Controller.getInstance().avvia(new JSONInterfaceSenderStrategy(),
				Integer.parseInt(Configurator.getInstance().getTimeout()));
				
				if(Configurator.getInstance().getPropValue("job.jsonif.report","false").equals("true"))
					Controller.getInstance().avvia(new JSONInterfaceReportStrategy(),
							Integer.parseInt(Configurator.getInstance().getPropValue("job.jsonif.report.scan_timeout","60")));

			}
		}catch( Exception e ) { 
			logger.error(e.getMessage(),e);
		}
	}
}
