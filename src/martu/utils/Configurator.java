/*
 * Copyright (c) 2019 Davide Martusciello
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package martu.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Configurator {

	private final Logger logger = LogManager.getLogger(Configurator.class);

	final String CONFIG_DIR = "config/";
	final String CONFIG_FILE_NAME = "configuration.properties";
	private String inizio;
	private String fine;
	private String timeout;
	private String inputPath;
	
	private boolean closeAtMidnight;
	private Properties prop;

	private HashMap<Integer, String> digitalDoxErrors;

	public String getInizio() {
		return inizio;
	}

	public String getFine() {
		return fine;
	}

	public String getTimeout() {
		return timeout;
	}

	public String getInputPath() {
		return inputPath;
	}
	
	public boolean getCloseAtMidnight() {
		return closeAtMidnight;
	}

	public void setCloseAtMidnight(boolean closeAtMidnight) {
		this.closeAtMidnight = closeAtMidnight;
	}	

	private static final Configurator instance = new Configurator();

	public static Configurator getInstance() {
		if (instance == null) {
			return new Configurator();
		}
		return instance;
	}

	public Configurator(){
		this.caricaDati();
	}

	private void caricaDati(){
		try{
			FileInputStream in = new FileInputStream (this.CONFIG_DIR + CONFIG_FILE_NAME);
			prop = new Properties();
//			prop.load(in);
			prop.load(new InputStreamReader(in, Charset.forName("UTF-8")));
			inizio = prop.getProperty("job.start","00:01");
			fine = prop.getProperty("job.end","23:59");
			timeout = prop.getProperty("job.scan_timeout","10");
			inputPath = prop.getProperty("job.input_dir_path");
			closeAtMidnight = Boolean.parseBoolean(prop.getProperty("job.close_at_midnight","false"));

			logger.info("***Properties***");
			logger.info("Begin job: " + inizio);
			logger.info("End job: " + fine);
			logger.info("timeout job: " + timeout);
			logger.info("close_at_midnight: " + closeAtMidnight);
			
			loadDigitalDoxErrorsMap();
			
			logger.info("***End of Properties***");
		}catch(FileNotFoundException e){
			logger.error(e.getMessage());
		}catch(IOException e){
			logger.error(e.getMessage());
		}
	}

	private void loadDigitalDoxErrorsMap() {
		logger.debug("Loading DigitalDox errors map");
		digitalDoxErrors = new HashMap<Integer,String>();
		try (BufferedReader br = new BufferedReader(new FileReader(this.CONFIG_DIR + "errors_map.txt"))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		    	try {
			        String[] values = line.split(";");
			        logger.debug("Adding " + values[0] + ":" + values[1]);
			        digitalDoxErrors.put(Integer.parseInt(values[0]), values[1]);
		    	}catch(Exception e) {
		    		logger.warn(e.getMessage(),e);
		    	}
		    }
		} catch (FileNotFoundException e) {
			logger.warn(e.getMessage(),e);
		} catch (IOException e) {
			logger.warn(e.getMessage(),e);
		}
	}

	public Calendar getInizioJob(){
		Calendar inizioCal = Calendar.getInstance();
		try{

			StringTokenizer st = new StringTokenizer(this.inizio,":");
			inizioCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(st.nextToken()));
			inizioCal.set(Calendar.MINUTE, Integer.parseInt(st.nextToken()));
			return inizioCal;
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		inizioCal.set(Calendar.HOUR_OF_DAY, 0);
		inizioCal.set(Calendar.MINUTE, 0);
		return inizioCal;
	}

	public Calendar getFineJob(){
		Calendar fineCal = Calendar.getInstance();
		try{
			StringTokenizer st = new StringTokenizer(this.fine,":");
			fineCal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(st.nextToken()));
			fineCal.set(Calendar.MINUTE, Integer.parseInt(st.nextToken()));
			return fineCal;
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		fineCal.set(Calendar.HOUR_OF_DAY, 0);
		fineCal.set(Calendar.MINUTE,1);
		return fineCal;
	}

	public String getPropValue(String propName, String defaultValue ) {
		
		String propValue = prop.getProperty(propName.toLowerCase(),defaultValue);
		if(propName.toLowerCase().contains("password")){
			logger.debug("Getting property '" + propName + "' with value '*****'");
			try {
				propValue= PasswordUtils.decrypt(propValue);
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
			}
		}else{
			logger.debug("Getting property '" + propName + "' with value '" + propValue + "'");
		}
		return propValue;
	}
	
	public int getPropValueInt(String propName, String defaultValue ) {
		int propValue = 0;
		try {
			propValue = Integer.parseInt(prop.getProperty(propName.toLowerCase(),defaultValue));
			logger.debug("Getting int property '" + propName + "' with value '" + propValue + "'");
		}catch(Exception e) {
			logger.error(e.getMessage(),e);
			propValue = Integer.parseInt(defaultValue);
		}
		return propValue;
	}

	public String getLogo(String logoCode) {
		String logo= getPropValue("pdf."+logoCode,"");
		if(logo.isEmpty()) {
			logo= getPropValue("pdf.logo_default","LOGO01.jpg");
		}
		return logo;
	}
	
	
	public HashMap<Integer,String> getDigitalDoxErrors(){
		return digitalDoxErrors;
	}
}
