/*
 * Copyright (c) 2019 Davide Martusciello
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package martu.utils;

import java.io.File;
import java.io.IOException;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CreaCartelle{
	private final Logger logger = LogManager.getLogger(CreaCartelle.class);
	private Calendar calendar;
	private int mese;
	private int anno;
	private String generatedDirPath;
	private boolean out;

	public String getGeneratedDirPath() {
		return generatedDirPath;
	}
	
	public int getMese() {
		return mese;
	}
	
	public int getAnno() {
		return anno;
	}
	
	public CreaCartelle(String path) {
		super();
		this.calendar = new GregorianCalendar();
		this.mese=calendar.get(Calendar.MONTH)+1;
		this.anno=calendar.get(Calendar.YEAR);
		this.generatedDirPath=path+ "/"+anno + "/" + mese + "/";
		try {
			createDir(path);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}
	}

	private boolean createDir(String path) throws IOException {
		if(!new File(path).exists()) {
			new File(path).mkdirs();
		}
		path=path+ "/"+anno;
		out = new File(path).mkdir();
		path=path+ "/"+mese;
		out = new File(path).mkdir();
		return out;
	}

}
