/*
 * Copyright (c) 2019 Davide Martusciello
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package martu.utils;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

public class FileFilterExtensionAndFirstChar implements FileFilter {

	private String extension;
	private ArrayList<String> prefixArray;

	public FileFilterExtensionAndFirstChar(String extension, ArrayList<String> prefixArray){
		this.extension="."+extension;
		this.prefixArray= prefixArray;
	}

	public boolean accept(File pathname) {
		if( pathname.getName().toLowerCase().endsWith(extension) )
			if (prefixArray.contains(pathname.getName().toUpperCase().substring(0,1)))
				return true;

		return false;
	}
}
