/*
 * Copyright (c) 2019 Davide Martusciello
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package martu.utils;
/***Not Tracked ***/
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import jcifs.util.Base64;

public class PasswordUtils {
	private static final String ALGHORITM = "AES";
	private static final String KEY = "<YOU 128BIT KEY>"; 
	public static String encrypt(String val)throws Exception{
		try
		{
			String text = val;
			Key aesKey = new SecretKeySpec(KEY.getBytes(), ALGHORITM);
			Cipher cipher = Cipher.getInstance(ALGHORITM);
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encrypted = cipher.doFinal(text.getBytes());
//			System.out.println(Base64.getEncoder().encodeToString(encrypted));
			return Base64.encode(encrypted);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	public static String decrypt(String val) throws Exception{
		try {
			Key aesKey = new SecretKeySpec(KEY.getBytes(), ALGHORITM);
			Cipher cipher = Cipher.getInstance(ALGHORITM);
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			String decrypted = new String(cipher.doFinal(Base64.decode(val).clone()));
//			System.out.println(decrypted);
			return decrypted;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	/* ENCRYPT YOUR PASSWORD */
	public static void main(String[] args) {
		try {
			
			String pwd= encrypt("*CURRENT");
			System.out.println(pwd);
			System.out.println(decrypt(pwd));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
